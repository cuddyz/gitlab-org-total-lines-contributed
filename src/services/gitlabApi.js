import http from './http'

async function getMRs(userName) {
  let MRs = []
  let page = 1

  do {
    const { data, headers } = await http().get(`api/gitlabMRs?author_username=${userName}&state=merged&page=${page}&per_page=100`)
    const MRsubset = data.map(mr => {
      return {
        sha: mr.merge_commit_sha ? mr.merge_commit_sha : mr.sha,
        project: mr.project_id
      }
    }).filter(mr => Boolean(mr.sha))

    MRs = MRs.concat(MRsubset)
    page = headers['x-next-page']
  } while (page)

  let addedLines = 0
  let removedLines = 0
  const promises = []

  MRs.forEach(MR => {
    promises.push(http().get(`api/gitlabLineCount/${MR.project}/repository/commits/${MR.sha}`))
  })

  await Promise.all(promises).then(responses => {
    responses.forEach(res => {
      addedLines += res.data.stats.additions
      removedLines += res.data.stats.deletions
    })
  })

  return { totalMRs: MRs.length, addedLines, removedLines }
}

export {
  getMRs
}
